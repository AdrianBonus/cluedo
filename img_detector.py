# import cv2
# import numpy as np
# from matplotlib import pyplot as plt
#
# img_to_detect = cv2.imread('mustard.png',0)
# img_to_detect_2 = img_to_detect.copy()
# template = cv2.imread('mustard_tmp.png',0)
# w, h = template.shape[::-1]
#
# #6 methods used for comparing
# methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
#             'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
#
# for each_method in methods:
#     img_to_detect = img_to_detect_2.copy()
#     method = eval(each_method)
#
#     #apply template matching
#     res = cv2.matchTemplate(img_to_detect,template,method)
#     min_val, max_val, min_lov, max_loc = cv2.minMaxLoc(res)
#
#     if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
#         top_left = min_loc
#     else:
#         top_left = max_loc
#     bottom_right = (top_left[0] + w, top_left[1] + h)
#
#     cv2.rectangle(img_to_detect,top_left, bottom_right, 255, 2)
#
#     plt.subplot(121),plt.imshow(res,cmap = 'gray')
#     plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
#     plt.subplot(122),plt.imshow(img_to_detect,cmap = 'gray')
#     plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
#     plt.suptitle(each_method)
#
#
#
#
#     plt.show()







# #https://pysource.com/2018/03/23/feature-matching-brute-force-opencv-3-4-with-python-3-tutorial-26/
# import cv2
# import numpy as np
#
# img1 = cv2.imread("mustard_tmp.png", cv2.IMREAD_GRAYSCALE)
# img2 = cv2.imread("mustard.png", cv2.IMREAD_GRAYSCALE)
#
# # ORB Detector
# orb = cv2.ORB_create()
# kp1, des1 = orb.detectAndCompute(img1, None)
# kp2, des2 = orb.detectAndCompute(img2, None)
#
# # Brute Force Matching
# bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
# matches = bf.match(des1, des2)
# matches = sorted(matches, key = lambda x:x.distance)
#
# matching_result = cv2.drawMatches(img1, kp1, img2, kp2, matches[:50], None, flags=2)
#
# cv2.imshow("Img1", img1)
# cv2.imshow("Img2", img2)
# cv2.imshow("Matching result", matching_result)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

#https://unix.stackexchange.com/questions/365592/how-to-rotate-a-set-of-pictures-from-the-command-line
#https://stackoverflow.com/questions/11541154/checking-images-for-similarity-with-opencv
#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_matcher/py_matcher.html
import cv2
import numpy as np
from matplotlib import pyplot as plt


def detect_image(input_frame,input_img):
    try:
        img = cv2.imread(input_img) #this should be the frame of the camera
        img2 = cv2.imread(input_frame) #this should go through a database that stores all of the pictures
        imgg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        sift = cv2.xfeatures2d.SIFT_create()

        kp1, des1 = sift.detectAndCompute(img,None)
        kp2, des2 = sift.detectAndCompute(img2,None)

        # FLANN parameters
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks=50)   # or pass empty dictionary

        flann = cv2.FlannBasedMatcher(index_params,search_params)

        matches = flann.knnMatch(des1,des2,k=2)

        # Need to draw only good matches, so create a mask
        matchesMask = [[0,0] for i in xrange(len(matches))]

        good_match = 0

        # ratio test as per Lowe's paper
        for i,(m,n) in enumerate(matches):
            if m.distance < 0.7*n.distance:
                matchesMask[i]=[1,0]
                good_match = good_match+1

        draw_params = dict(matchColor = (0,255,0),
                           singlePointColor = (255,0,0),
                           matchesMask = matchesMask,
                           flags = 0)

        img3 = cv2.drawMatchesKnn(img,kp1,img2,kp2,matches,None,**draw_params)

        if good_match > 500:
            print input_img[:-4]
            plt.imshow(img3,),plt.show()
            print "Number of matched points :", good_match
    except:
        pass

input_frame = ["mustard.png", "peacock.png", "plum.png", "revolver.png", "rope.png", "scarlet.png", "wrench.png"]
input_img = raw_input("Please enter an file name that would represent the frame from the camera feed : ")
for i in range(0, len(input_frame)):
    detect_image(input_img, "images/"+input_frame[i])
